# Projet Algorithme : 
## Description : 
Le script contient deux classes : TextVect et KNNClasses.

La classe TextVect est utilisée pour représenter des vecteurs de texte, des dictionnaires qui contiennent des mots et leur nombre d'occurrences dans un texte. 

Cette classe a 03 méthodes :

La méthode "doc2vec" qui prend en entrée le nom d'un fichier texte et renvoie une liste d'objets TextVect représentant chaque paragraphe du texte.

La méthode "filter" qui permet de filtrer les vecteurs en excluant certains mots (stopwords) et les mots qui n'apparaissent qu'une fois (hapax).

La méthode "tf_idf" qui calcule la mesure TF-IDF pour chaque mot dans une liste de textes et renvoie un dictionnaire où chaque clé est un mot et chaque valeur est une liste de valeurs TF-IDF correspondant à chaque texte.

La classe KNNClasses est utilisée pour implémenter un classificateur KNN. 

Cette classe a 08 méthodes:

- add_class: pour ajouter une nouvelle classe

- add_vector: pour ajouter un vecteur à une classe définie par un label

- del_class: pour supprimer la classe correspondant à label

- save_as_json: pour enregistrer les données d'une classe au format json

- load_as_json: pour charger les données depuis une classe au format json

- classify: renvoie la liste des classes candidates pour le vecteur, comme liste triée de paires par similarité décroissante, la similarité étant la moyenne des similarités obtenues sur les vecteurs retenus pour la classe correspondante dans les k plus proches voisins. La fonction sim_func sera passée en paramètre facultatif, et correspondra par défaut à un calcul de cosinus.

- sim_cosinus: calcule la similarité cosinus entre deux vecteurs.

- nearest_vectors: trouver les k vecteurs les plus proches d'un vecteur de référence dans une liste de vecteurs donnée.

***La "Main" est utilisée pour tester le jeu de données.***

## Les bogues et les améliorations : 

Dans la méthode filter(), les arguments stopwords et hapax sont définis comme des listes sans spécifier leur type cela pourrait entraîner une erreur si un autre type est utilisé.

Dans la méthode tf_idf(), si un mot n'apparaît dans aucun des textes, une erreur se produira lors du calcul car ça valeur sera égale à 0. 


Dans la méthode doc2vec(), le comptage de mots ne prend pas en compte la casse. Il serait peut-être utile de convertir tous les mots dans un format spécifier (tout en miniscule par exemple) pour éviter les doublons causés par des mots avec des cas différents.

*NB: pour chaque class et fonction, vous trouverez une partie commentaire et une partie API pour mieux expliquer et détailler le script.* 