from typing import List, Dict , Tuple
from math import sqrt, log 
import json 

class TextVect:

    #initialisation de la classe TextVect : 
    """
    Classe TextVect représente un vecteur texte.

    Args:
        values (Dict[str, float]): un dictionnaire contenant les occurrences de chaque mot dans un texte.
    """
    def __init__(self, values: Dict[str, float]):
        self.values = values

    #doc2vec permet de transformer un fichier de texte en une liste d'objets TextVect
    #chaque objet représentant un paragraphe du fichier, et contenant les occurrences de chaque mot dans le paragraphe.
    """
        Méthode de classe qui transforme un fichier de texte en une liste d'objets TextVect. 
        Chaque objet représente un paragraphe du fichier, et contient les occurrences de chaque mot dans le paragraphe.

        Args:
            filename (str): Le nom du fichier à transformer.

        Returns:
            List["TextVect"]: Une liste d'objets TextVect.
    """
    @classmethod
    def doc2vec(cls, filename: str) -> List["TextVect"]:
        #ouvrir le fichier en mode lecture 
        with open(filename, mode="r", encoding="utf-8") as f:
            text = f.read()
        #compter le nombre de mots dans les textes 
        word_count = {}
        for word in text.split():
            if word not in word_count:
                word_count[word] = 1
            else:
                word_count[word] += 1
        texts = text.split("\n\n")
        result = []
        for text in texts:
            values = {word: word_count[word] for word in text.split()}
            result.append(cls(values))
        return result

    #filter prend en entrée une liste de mots à exclure (stopwords) et une liste de mots qui n'apparaissent qu'une fois dans le corpus (hapax). 
    #Elle filtre le vecteur en ne gardant que les mots qui ne sont pas dans ces listes.
    """
        Méthode qui filtre le vecteur en ne gardant que les mots qui ne sont pas dans les listes de stopwords et de hapax.

        Args:
            stopwords (List[str]): Liste de mots à exclure.
            hapax (List[str]): Liste de mots qui n'apparaissent qu'une fois dans le corpus.
    """
    def filter(self, stopwords: list[str], hapax: list[str]):
        self.values = {key: value for key, value in self.values.items()
                       if key not in stopwords and key not in hapax}
    
    #tf_idf calcule TF-IDF pour chaque mot dans une liste de textes (une liste de listes de mots). 
    #La méthode renvoie un dictionnaire où les clés sont les mots et les valeurs sont des listes TF-IDF correspondantes pour chaque texte.
    """
        Méthode qui calcule TF-IDF pour chaque mot dans une liste de textes (une liste de listes de mots). 
        La méthode renvoie un dictionnaire où les clés sont les mots et les valeurs sont des listes TF-IDF correspondantes pour chaque texte.

        Args:
            texts (List[List[str]]): Une liste de textes (une liste de listes de mots).

        Returns:
            Dict[str, List[float]]: Un dictionnaire où les clés sont les mots et les valeurs sont des listes TF-IDF correspondantes pour chaque texte.
    """
    def tf_idf(self, texts: List[List[str]]) -> Dict[str, List[float]]:
        tf = {}
        idf = {}
        for text in texts:
            for word in text:
                tf.setdefault(word, []).append(text.count(word) / len(text))
                idf[word] = idf.get(word, 0) + 1
        for word in idf:
            idf[word] = log(len(texts) / (1 + idf[word]))

        return {word: [tf_value * idf[word] for tf_value in tf[word]] for word in tf}

class KNNClasses:
    vect=dict

    #initialisation de la classe KNNclasses
    """
        Classe KNNClasses qui implémente un classificateur KNN.

        Args:
            description (str, optional): Description de l'objet KNNClasses.
    """
    
    def __init__(self, description=""):
        self.description = description
        self.data = []
    
    #Ajouter un dictionnaire (label, vecteurs) à la liste data 
    """
        Méthode qui ajoute un dictionnaire (label, vecteurs) à la liste data.

        Args:
            label (str): Label de la classe à ajouter.
            vectors (List[Dict[str, float]]): Une liste de vecteurs TextVect correspondant à la classe à ajouter.
    """
    def add_class(self, label:str, vectors:list):
        self.data.append({"label": label, "vectors": vectors})
        
    #Ajouter un vecteur à la liste de vecteurs correspondant à une classe donnée dans la liste data
    """
        Méthode qui ajoute un vecteur à la liste de vecteurs correspondant à une classe donnée dans la liste data.

        Args:
            label (str): Label de la classe.
            vector (Dict[str, float]): Le vecteur TextVect à ajouter.
    """
    def add_vector(self, label:str, vector:vect):
        for item in self.data:
            if item["label"] == label:
                item["vectors"].append(vector)
                return
    #Supprimer une classe figurant dans la liste data 
    """
        Méthode qui supprime une classe figurant dans la liste data.

        Args:
            label (str): Label de la classe à supprimer.
    """
    def del_class(self, label:str):
        for item in self.data:
            if item["label"] == label:
                self.data.remove(item)
                return
            
    #Sauvgarder le contenu de la liste data sous forme d'un fichier Json
    def save_as_json(self, filename):
        with open(filename, "w") as f:
            json.dump(self.data, f)
            
    #Charger les données du fichier Json
    def load_as_json(self, filename):
        with open(filename, "r") as f:
            self.data = json.load(f)

    # classify prend en entrée un vecteur à classifier, un paramètre k pour spécifier le nombre de voisins les plus proches 
    # une fonction de similarité sim_func à utiliser. Si sim_func n'est pas spécifiée, la fonction par défaut est utilisée, qui calcule la similarité cosinus.
    def classify(self, vector, k, sim_func=None):

        if sim_func is None:
            sim_func = self.sim_cosinus  # si sim_func est None, on utilise sim_cosinus par défaut
            
        class_scores = {}  # on initialise un dictionnaire vide pour stocker les scores de chaque classe
        
        # On itère sur toutes les classes dans la liste data
        for item in self.data:
            label = item["label"]  # on récupère le label de la classe
            vectors = item["vectors"]  # on récupère les vecteurs de la classe
            
            # On cherche les k vecteurs les plus proches du vecteur à classifier en utilisant
            # la fonction nearest_vectors avec la fonction de similarité spécifiée.
            # Cette fonction renvoie une liste de k vecteurs.
            nearest_vectors = self.nearest_vectors(vectors, vector, k, sim_func)
            
            # On calcule la similarité moyenne entre le vecteur à classifier et les k vecteurs
            # les plus proches en utilisant la fonction de similarité spécifiée.
            mean_sim = sum([sim_func(vector, nv) for nv in nearest_vectors])/k
            
            # On ajoute le score moyen de la classe au dictionnaire class_scores avec le label
            # de la classe comme clé.
            class_scores[label] = mean_sim
            
        # On trie les scores par ordre décroissant et on renvoie la liste triée
        sorted_scores = sorted(class_scores.items(), key=lambda x: x[1], reverse=True)
        return sorted_scores

    # Cette fonction calcule la similarité cosinus entre deux vecteurs v1 et v2. 
    def sim_cosinus(self, v1, v2):

        # Calcul du produit scalaire entre v1 et v2.
        dot_product = sum([v1[k]*v2[k] for k in set(v1.keys()).intersection(set(v2.keys()))])
        
        # Calcul de la norme de v1.
        norm_v1 = sqrt(sum([v1[k]**2 for k in v1.keys()]))
        
        # Calcul de la norme de v2.
        norm_v2 = sqrt(sum([v2[k]**2 for k in v2.keys()]))
        
        # Vérification si l'une des deux normes est nulle.
        if norm_v1 == 0 or norm_v2 == 0:
            # Si l'une des deux normes est nulle, retourne 0 car la similarité ne peut pas être calculée.
            return 0
        
        # Calcul de la similarité cosinus entre v1 et v2.
        return dot_product / (norm_v1 * norm_v2)

    #nearest_vectors permet de trouver les k vecteurs les plus proches d'un vecteur de référence dans une liste de vecteurs
    def nearest_vectors(self, vectors, vector, k, sim_func):
        
        distances = [(v, sim_func(v, vector)) for v in vectors]
        # On calcule les distances entre chaque vecteur de la liste vectors et le vecteur de référence en utilisant la fonction sim_func
        # La variable distances est une liste de tuples où chaque tuple contient un vecteur et sa distance par rapport au vecteur de référence
        
        sorted_distances = sorted(distances, key=lambda x: x[1], reverse=True)
        # On trie la liste distances par ordre décroissant de distance en utilisant la fonction sorted() et la clé lambda x: x[1]
        # Cela crée une nouvelle liste triée de tuples contenant les vecteurs de la liste vectors classés par ordre de distance décroissante
        
        return [v[0] for v in sorted_distances[:k]]
        # On retourne les k premiers vecteurs de la liste triée en utilisant la syntaxe de slicing [:k]
        #La liste retournée ne contient que les vecteurs de la liste vectors classés par ordre de distance décroissante par rapport au vecteur de référence
#test de fonctions
def main():
    # Chargement des recettes depuis un fichier texte
    doc1 = TextVect.doc2vec("C:/Users/djaam/Desktop/Algorithmes/Examen/recettes.txt")
    recette1 = doc1[0]
    recette2 = doc1[1]
    recette3 = doc1[2]

    # Création des textes d'exemple
    texte1 = list(recette1.values.keys())
    texte2 = list(recette2.values.keys())
    texte3 = list(recette3.values.keys())

    texts = [texte1, texte2, texte3]

    # Création des vecteurs TextVect à partir des textes
    vectors = [TextVect({word: count}) for count, word in zip((recette1.values.values(), recette2.values.values(), recette3.values.values()), ("ingredients", "beurre", "farine"))]

    # Ouverture et lecture du fichier contenant les recettes
    with open("C:/Users/djaam/Desktop/Algorithmes/Examen/recettes.txt", "r") as f:
        recettes = f.readlines()

    # Affichage des recettes
    print("Recettes :")
    for i, recette in enumerate(recettes):
        print(f"Recette {i+1} : {recette.strip()}")

    # Affichage des textes et des vecteurs créés à partir des recettes
    print("Textes:")
    print(texts)

    print("Vecteurs TextVect :")
    print(vectors)

    # Création d'une instance de la classe KNNClasses avec la description "Classification de fruits"
    knn = KNNClasses(description="Classification de fruits")

    # Ajout de deux classes : "pomme" et "orange"
    knn.add_class("pomme", [
        {"couleur": 0.8, "taille": 0.5},
        {"couleur": 0.7, "taille": 0.6},
        {"couleur": 0.9, "taille": 0.4},
        {"couleur": 0.6, "taille": 0.7},
        {"couleur": 0.75, "taille": 0.65},
    ])
    knn.add_class("orange", [
        {"couleur": 0.3, "taille": 0.8},
        {"couleur": 0.4, "taille": 0.9},
        {"couleur": 0.2, "taille": 0.95},
        {"couleur": 0.35, "taille": 0.85},
        {"couleur": 0.25, "taille": 0.92},
    ])

    # Ajout d'un nouveau vecteur "pomme"
    knn.add_vector("pomme", {"couleur": 0.7, "taille": 0.7})

    # Suppression de la classe "orange"
    knn.del_class("orange")

    # Sauvegarde des données au format JSON
    knn.save_as_json("data.json")

    # Chargement des données depuis un fichier JSON
    knn.load_as_json("data.json")

    # Classification d'un nouveau vecteur
    KNN = knn.classify({"couleur": 0.8, "taille": 0.6}, k=3)
    print(KNN)
#appeller la fonction main
if __name__ == "__main__":
    main()

